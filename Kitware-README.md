# Adapting R-C3D to the DIVA Framework

This is a fork of [Action Tubelet Detector Repository](https://github.com/vkalogeiton/caffe/tree/act-detector).  The purpose of this fork is to implement the ACT algorithm in terms of the [DIVA Framework](https://github.com/Kitware/DIVA).  The DIVA Framework is an open source framework for deploying video activity recognition analytics in a multi-camera environment funded by [IARPA](https://www.iarpa.gov/).  

## DIVA Framework Resources

* [DIVA Framework Github Repository](https://github.com/Kitware/DIVA) This is the main DIVA Framework site, all development of the framework happens here.
* [DIVA Framework Issue Tracker](https://github.com/Kitware/DIVA/issues)  Submit any bug reports or feature requests for the framework here.
* [DIVA Framework Main Documentation Page](https://kwiver-diva.readthedocs.io/en/latest/>) The source for the framework documentation is maintained in the Github repository using [Sphinx](http://www.sphinx-doc.org/en/master/)  A built version is maintained on [ReadTheDocs](https://readthedocs.org/).   A good place to get started in the documentation, after reading the [Introduction](https://kwiver-diva.readthedocs.io/en/latest/introduction.html) is the [UseCase](https://kwiver-diva.readthedocs.io/en/latest/usecases.html) section which will walk you though a number of typical use cases with the framework.

## Guide to this Adaptation

Detailed technical documentation on this adaption can be found at [this location](https://act-detector.readthedocs.io/en/kitware-master/processes.html)

The Sprokit pipeline that runs the ACT process can be found in [pipelines/act.pipe](https://gitlab.kitware.com/kwiver/act_detector/blob/kitware/master/pipelines/act.pipe)

The code that implements the core ACT Sprokit process can be found in [processes/act_detector.py](https://gitlab.kitware.com/kwiver/act_detector/blob/kitware/master/processes/act_detector.py)

A [Dockerfile](https://docs.docker.com/engine/reference/builder/) that creates a container in which the DIVA Framework is built and the ACT process runs can be found [docker/standalone/gpuDockerfile](https://gitlab.kitware.com/kwiver/act_detector/tree/kitware/master/docker/standalone/gpu).  That is a reference to an NVIDIA/GPU based version, other Dockerfiles for different platoforms can be found in the "docker" subdirectory as well.
