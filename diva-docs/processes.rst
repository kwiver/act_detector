KWIVER Processes in ACT Detector
================================

ACT :cite:`kalogeiton17iccv`

.. autoclass:: act_detector.ACTDetector

.. autoclass:: act_json_writer.ACTJsonWriter

.. autoclass:: act_visualizer.ACTVisualizer

.. autoclass:: modify_bbox_resolution.ModifyBboxResolution

.. autoclass:: merge_tubes.MergeTubes

.. bibliography:: processes.bib
