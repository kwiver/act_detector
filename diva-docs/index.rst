.. ACT Detector documentation master file, created by
   sphinx-quickstart on Fri Feb  8 14:27:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ACT Detector's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   processes
   pipelines



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
