Pipelines in ACT Detector
=========================

ACT Activity Detection Pipeline
-------------------------------

.. graphviz:: _pipe/act.dot

ACT Activity Object Detection Pipeline
--------------------------------------

.. graphviz:: _pipe/act_aod.dot

ACT ZMQ Activity Detection Pipeline
-----------------------------------

.. graphviz:: _pipe/act_zmq.dot

ACT ZMQ Activity Object Detection Pipeline
------------------------------------------

.. graphviz:: _pipe/act_aod_zmq.dot

