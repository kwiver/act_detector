# DockerFile for ACT

The folder contains the docker file to build ACT with DIVA

## Dependencies
1. [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Nvidia Docker 2](https://github.com/NVIDIA/nvidia-docker)

## Build
### ACT for Activity Detection
    nvidia-docker build -t kitware/act-ad -f Dockerfile.AD .
### ACT for Activity Object Detection
    nvidia-docker build -t kitware/act-aod -f Dockerfile.AOD .

## Run The Container
### ACT for Activity Detection
    nvidia-docker run -ti --name act-ad \
          --mount type=bind,source=<path to activity-index directory>,target=/data \
          --mount type=bind,source=<path to visualization directory>,target=/experiment/output \
          kitware/act-ad:latest
### ACT for Activity Object Detection
    nvidia-docker run -ti --name act-aod \
          --mount type=bind,source=<path to activity-index directory>,target=/data \
          --mount type=bind,source=<path to visualization directory>,target=/experiment/output \
          kitware/act-aod:latest

### Note: modify the source in --mount to the directory with activity-index.json.


### Note: Further instructions related to virat based experiments are available in virat-act-detector-scripts on kitware/master.
