#
# Pipe file for ACT AOD
#

# Image configuration
config input
  exp_file = etc/rc3d_experiment.yml
  
# Algorithm configuration
config algo 
 exp_file = /diva/act-detector/virat-act-detector-scripts/rgb_docker.yml

# -----------------------------------------------------------------
process exp :: diva_experiment
  experiment_file_name = $CONFIG{input:exp_file}

# ------------------------------------------------------------------
process opt_flow :: optical_flow
  output_image_width=300
  ouptut_image_height=300

connect from exp.image to opt_flow.image
connect from exp.timestamp to opt_flow.timestamp

# ------------------------------------------------------------------
process act_detector :: ACTDetector
  exp = $CONFIG{algo:exp_file}

connect from exp.file_name to act_detector.file_name
connect from exp.image to act_detector.rgb_image
connect from opt_flow.image to act_detector.flow_image
connect from exp.timestamp to act_detector.timestamp

# -------------------------------------------------------------------
process merge_tubes :: MergeTubes
  exp = $CONFIG{algo:exp_file}
  
connect from exp.file_name to merge_tubes.file_name
connect from act_detector.object_track_set to merge_tubes.object_track_set
connect from exp.timestamp to merge_tubes.timestamp


# -------------------------------------------------------------------
process yolo_v2
  :: image_object_detector
  :detector:type    darknet

  # Network config, weights, and names
  :detector:darknet:net_config   /diva/darknet/virat_auto.inference.cfg
  :detector:darknet:weight_file  /diva/darknet/virat_auto_final.weights
  :detector:darknet:class_names  /diva/darknet/models/virat.names

  # Detector parameters
  :detector:darknet:thresh       0.50
  :detector:darknet:hier_thresh  0.5
  :detector:darknet:gpu_index    1

  # Image scaling parameters
  #:detector:darknet:resize_option maintain_ar
  :detector:darknet:resize_ni     1024
  :detector:darknet:resize_nj     1024
  #:detector:darknet:scale         1.0

connect from exp.image to yolo_v2.image


# -------------------------------------------------------------------
process expand_bbox :: ModifyBboxResolution

connect from yolo_v2.detected_object_set to expand_bbox.detected_object_set 

# -------------------------------------------------------------------
process json_writer :: ACTJsonWriter
  exp = $CONFIG{algo:exp_file}
  is_aod = True 
  confidence_threshold = 0.1
  json_path = sysfile_vis.json
  
connect from exp.file_name to json_writer.file_name
connect from merge_tubes.object_track_set to json_writer.object_track_set
connect from exp.timestamp to json_writer.timestamp
connect from expand_bbox.detected_object_set to json_writer.detected_object_set

# ------------------------------------------------------------------
# use python scheduler
config _scheduler
  type = pythread_per_process

config _pipeline:_edge
  capacity = 1     # set default edge capacity
