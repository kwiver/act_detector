#+
# Setup script for ACT
# Usage:
# source setup_ACT.sh <DIVA_INSTALL_DIR>
#
# DIVA_INSTALL_DIR: Directory where DIVA is installed
#

DIVA_INSTALL_DIR=$1
if [ -z "$DIVA_INSTALL_DIR" ]
then
  echo "Usage: source setup_ACT.sh <DIVA_INSTALL_DIR>"
else
  source ${DIVA_INSTALL_DIR}/setup_DIVA.sh
  SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  export PYTHONPATH=${SCRIPT_DIR}:${SCRIPT_DIR}/act-detector-scripts:${PYTHONPATH}
  export PYTHONPATH=${SCRIPT_DIR}/virat-act-detector-scripts:${SCRIPT_DIR}/python:$PYTHONPATH
  export SPROKIT_PYTHON_MODULES=processes:$SPROKIT_PYTHON_MODULES
fi
